extends Node3D

class_name Cannon

var projectile_scene : PackedScene
var projectile : ScriptedProjectile

func set_projectile(projectile_ref_scene : PackedScene) -> void:
	projectile_scene = projectile_ref_scene

func load_projectile() -> void:
	projectile = projectile_scene.instantiate()

func aim(target : StaticBody3D) -> void:
	load_projectile()
	var target_rotation = -PI * 0.5 + projectile.get_launching_angle(position.z - target.position.z, position.y - target.position.y, false)
	if is_nan(target_rotation):
		print("no fire solution found")
	else:
		rotation.x = target_rotation

func shoot():
	load_projectile()
	$Muzzle.add_child(projectile)
	projectile.launch()
	return projectile
