extends Node3D

@onready var cannon : Cannon = $cannon
#@onready var targets : Array = [$TestTarget1, $TestTarget2, $TestTarget3, $TestTarget4]
@onready var targets : Array = [$TestTarget4]
@onready var cam : Camera3D = $Camera3D
var torpedo_scene : PackedScene = preload("res://projectiles/test_torpedo_projectile.tscn")

var time_passed : float = 0
var current_projectile = null

func _ready():
	var torpedo = torpedo_scene.instantiate()
	cannon.add_child(torpedo)
	torpedo.global_rotation.x = -PI / 2
	torpedo.launch()
	cannon.set_projectile(preload("res://projectiles/test_scripted_ballistic_projectile.tscn"))

func _physics_process(_delta):
	if !targets.is_empty() && !current_projectile is ScriptedProjectile:
		var next_target = targets.pop_front()
		cannon.aim(next_target)
		current_projectile = cannon.shoot()

func _process(_delta):
	if current_projectile is ScriptedProjectile:
		if current_projectile.position.y > -0.1:
			cam.position.y = current_projectile.position.y + 7
			cam.position.z = current_projectile.position.z
		else:
			print("impact: " + str(-current_projectile.position.z))
			current_projectile = null
	$Label.text = str(Engine.get_frames_per_second()) + " fps"
