extends ScriptedBallisticProjectile

func _on_area_3d_body_entered(body):
	if body is TestTarget:
		body.hit.emit()
