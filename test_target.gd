extends StaticBody3D

class_name TestTarget

signal hit

var was_hit : bool = false

func _ready():
	hit.connect(_on_hit)

func _on_hit():
	if was_hit:
		print(name + " hit multiple times!")
		$SpotLight3D.light_color = Color(1, 0, 0)
	else:
		$SpotLight3D.light_color = Color(0, 1, 0)
		was_hit = true
