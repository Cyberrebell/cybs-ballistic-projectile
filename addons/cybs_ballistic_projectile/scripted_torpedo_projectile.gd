class_name ScriptedTorpedoProjectile
extends ScriptedProjectile

func prepare() -> void:
	start_position = position

func _physics_process(delta):
	if launched:
		velocity.y -= gravity * delta
		move_and_slide()
		process_update(delta)

func move_by_time() -> void:
	var next_position = calculate_position_at_time(time)
	position.x = next_position.x
	position.z = next_position.z
