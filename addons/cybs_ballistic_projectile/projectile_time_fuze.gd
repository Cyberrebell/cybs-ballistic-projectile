class_name ProjectileTimeFuze
extends Node3D

@onready var projectile: ScriptedProjectile = get_parent()
@export var trigger_distance = 1000
var trigger_time = 2
var trigger_position: Vector3

signal trigger

func _ready():
	projectile.projectile_launched.connect(_on_projectile_launch)

func _on_projectile_launch() -> void:
	trigger_time = projectile.calculate_time_to_z_position(trigger_distance)
	trigger_position = projectile.calculate_position_at_time(trigger_time)

func _physics_process(_delta):
	if weakref(projectile).get_ref() is ScriptedProjectile:
		if projectile.time > trigger_time:
			trigger_time = 1000000
			trigger.emit()
	else:
		queue_free()
