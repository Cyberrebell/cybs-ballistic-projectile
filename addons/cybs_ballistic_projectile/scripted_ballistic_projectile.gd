class_name ScriptedBallisticProjectile
extends ScriptedProjectile

func _process(delta):
	process_update(delta)

func _ready() -> void:
	prepare()

func prepare() -> void:
	start_position = position
	set_process(false)

func launch() -> void:
	look_at(to_global(Vector3.UP))
	super.launch()
	set_process(true)

func move_by_time() -> void:
	var next_position = calculate_position_at_time(time)
	var vel = (next_position - position).rotated(Vector3.UP, -start_rotation_y).rotated(Vector3.RIGHT, -start_rotation_z)
	rotation = Vector3(Vector2(vel.z, vel.y).angle_to(Vector2.DOWN), start_rotation_y, start_rotation_z)
	move_and_collide(next_position - position)
	if position.y < -50:
		queue_free()

func calculate_time_to_z_position(z: float) -> float:
	return z / (muzzle_velocity * cos(start_angle))

func calculate_position_at_time(target_time: float) -> Vector3:
	return start_position + Vector3(
		0,
		muzzle_velocity * sin(start_angle) * target_time + sign(start_rotation_z - PI / 2) * gravity * 0.5 * pow(target_time, 2),
		-muzzle_velocity * cos(start_angle) * target_time
	).rotated(Vector3.RIGHT, start_rotation_z).rotated(Vector3.UP, start_rotation_y)
