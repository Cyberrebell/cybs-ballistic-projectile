class_name ScriptedProjectile
extends CharacterBody3D

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

@export var damage: int = 20
@export var muzzle_velocity: float = 50

@export var delete_time = 10

var start_position: Vector3
var start_angle: float
var start_rotation_y: float
var start_rotation_z: float
var launched: bool = false
var time = 0

signal projectile_launched
signal hit_target
signal event_triggered

func process_update(delta):
	if launched:
		time += delta
		move_by_time()
		if time > delete_time:
			queue_free()

func prepare() -> void:
	pass

func launch() -> void:
	top_level = true
	start_angle = rotation.x
	start_rotation_y = rotation.y
	start_rotation_z = rotation.z
	prepare()
	launched = true
	projectile_launched.emit()

func move_by_time() -> void:
	pass

func get_launching_angle(distance: float, height_offset: float, steep: bool):
	return calculate_launching_angle(distance, muzzle_velocity, gravity, height_offset, steep)

static func calculate_launching_angle(distance: float, projectile_muzzle_velocity: float, gravity: float, height_offset: float, steep: bool) -> float:
	var speed2: float = pow(projectile_muzzle_velocity, 2)
	var root: float = pow(projectile_muzzle_velocity, 4) - gravity * (gravity * pow(distance, 2) + 2 * -height_offset * speed2)
	if root < 0:
		return NAN
	var variant: int = -1
	if steep:
		variant = 1
	return atan2(speed2 + variant * sqrt(root), gravity * distance)

static func calculate_projectile_distance(launching_angle: float, projectile_muzzle_velocity: float, gravity: float) -> float:
	return sin(launching_angle * 2) * pow(projectile_muzzle_velocity, 2) / gravity

static func calculate_projectile_distance_height_offset(launching_angle: float, projectile_muzzle_velocity: float, gravity: float, height_offset: float) -> float:
	return projectile_muzzle_velocity * cos(launching_angle) * (projectile_muzzle_velocity * sin(launching_angle) + sqrt(pow(projectile_muzzle_velocity * sin(launching_angle), 2) + 2 * gravity * height_offset)) / gravity

func calculate_time_to_z_position(z: float) -> float:
	return z / muzzle_velocity

func calculate_position_at_time(target_time: float) -> Vector3:
	return start_position + Vector3(
		0,
		0,
		-muzzle_velocity * target_time
	).rotated(Vector3.UP, start_rotation_y)
